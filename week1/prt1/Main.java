package week1.prt1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Employee emp1 = new Employee(s.next(), s.nextInt(), s.nextInt());
        Employee emp2 = new Employee(s.next(), s.nextInt());
        emp2.setHours(s.nextInt());

        Employee emp3 = new Employee();
        emp3.setName("Ivan");
        emp3.setHours(6);
        emp3.setRate(9);

        System.out.println(emp1.toString());
        System.out.println(emp2.toString());
        System.out.println(emp3.toString());

    }
}

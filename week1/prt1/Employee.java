package week1.prt1;

public class Employee {
    private String name;
    private int rate;
    private int hours;

    static int totalSum;

    public Employee() {}

    public Employee(String name, int rate) {
        this.name = name;
        this.rate = rate;
    }

    public Employee(String name, int rate, int hours) {
        this.name = name;
        this.rate = rate;
        this.hours = hours;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public static void setTotalSum(int totalSum) {
        Employee.totalSum = totalSum;
    }

    public String getName() {
        return name;
    }

    public int getRate() {
        return rate;
    }

    public int getHours() {
        return hours;
    }

    public static int getTotalSum() {
        return totalSum;
    }

    public int salary() {
        return this.rate*this.hours;
    }
    public String toString() {
        return "Employee [name="+name+", rate="+rate+", hours="+hours+"]";
    }
    public void changeRate(int rate) {
        this.rate=rate;
    }
    public double bonuses() {
        int s = this.salary();
        return s*0.1;
    }
}

package week1.ht1;

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person("Venya", 2000);
        Person p2 = new Person();
        p2.setName("Ivan");
        p2.setBirthYear(2000);

        Person p3 = new Person();
        Person p4 = new Person();
        Person p5 = new Person();

        p3.input();
        p4.input();
        p5.input();

        p4.changeName("Alex");

        p1.output();
        p2.output();
        p3.output();
        p4.output();
        p5.output();
    }

}

package week1.ht1;

import java.util.Calendar;
import java.util.Scanner;

public class Person {
    private String name;
    private int birthYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Person() {}
    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public int age() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year-birthYear;
    }

    public void input(){
        Scanner s = new Scanner(System.in);
        this.name = s.next();
        this.birthYear = s.nextInt();
    }

    public void output(){
        System.out.println("Employee [name="+name+", birthYear="+birthYear+", age="+this.age()+"]");
    }
    public void changeName(String newname) {
        this.name = newname;
    }

}
